package org.megadevs.hashcode.painting;

/**
 * Created by ziby on 06/02/16.
 */
public class CommandPaintLine extends Command {
    int r1;
    int c1;
    int r2;
    int c2;

    public CommandPaintLine(int r1, int c1, int r2, int c2) {
        this.r1 = r1;
        this.c1 = c1;
        this.r2 = r2;
        this.c2 = c2;
    }

    @Override
    public String toString() {
        return "PAINT_LINE " + r1 + " " + c1 + " " + r2 + " " + c2;
    }

}
