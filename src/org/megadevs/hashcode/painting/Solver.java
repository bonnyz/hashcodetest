package org.megadevs.hashcode.painting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by ziby on 06/02/16.
 */
public class Solver {

    InputImage input;
    Solution sol;
    Random randomGenerator = new Random();

    public Solver(InputImage input){
        this.input = input;
    }

    public Solution getSolution(){
        return sol;
    }

    public void elaborateBaseSolution(){
        System.out.println("Elaborate solution");
        sol = new Solution();

        //Base: tutti comandi da 1 pixel
        for(int i = 0; i<input.r; i++){
            for(int j = 0; j<input.c; j++){
                if(input.isPainted(i, j))
                    sol.addCommand(new CommandPaintSquare(i,j,0));
            }
        }
    }

    public Solution generateRandomSolution(Solution fromSol, float percentageToKeep, boolean useVertical, boolean useHorizontal, boolean useSquare, boolean useErase){
        Solution tmpSol = new Solution();
        boolean used[][];
        if(fromSol != null){
            int commandsToKeep = (int) (fromSol.getCommands().size() * percentageToKeep);
            Collections.shuffle(fromSol.getCommands());
            for(int i=0; i<commandsToKeep; i++){
                tmpSol.addCommand(fromSol.getCommands().get(i));
            }
            used = getUsedMap(tmpSol);
        }
        else
            used = new boolean[input.r][input.c];

        long tic = System.currentTimeMillis();
        int commandType;

        while(!checkValidity(tmpSol)){
            commandType = randomGenerator.nextInt(3);
            int randomStart_r = 0;//randomGenerator.nextInt(input.r);
            int randomStart_c = 0;//randomGenerator.nextInt(input.c);
            if(commandType == 0 && useVertical){ //line vertical
                //Vertical
                boolean findingCommand = true;
                for(int i = randomStart_c; i < input.c && findingCommand; i++){
                    int lineStart = -1;
                    int lineEnd = -1;
                    for(int j = randomStart_r; j < input.r && findingCommand; j++){
                        if(input.isPainted(j, i) && !used[j][i]){
                            if(lineStart == -1){
                                lineStart = lineEnd = j;
                            }
                            else
                                lineEnd = j;

                            used[j][i] = true;
                        }
                        else{
                            if(lineStart != -1){
                                if(lineEnd != lineStart){
                                    CommandPaintLine c = new CommandPaintLine(lineStart, i, lineEnd, i);
                                    tmpSol.addCommand(c);
                                    //System.out.println("Added VERTICAL " +c.toString());
                                    findingCommand = false;
                                }else
                                    used[lineStart][i] = false;

                                lineStart = -1;
                                lineEnd = -1;
                            }
                        }
                    }

                    if(lineStart != -1){
                        if(lineEnd != lineStart){
                            CommandPaintLine c = new CommandPaintLine(lineStart, i, lineEnd, i);
                            tmpSol.addCommand(c);
                            //System.out.println("Added VERTICAL " +c.toString());
                            findingCommand = false;
                        }
                        else
                            used[lineStart][i] = false;
                    }
                }

                if(findingCommand)
                    useVertical = false;
            }
            else if(commandType == 1 && useHorizontal){ //line horizontal

                //Horizontal
                boolean findingCommand = true;
                for(int i = randomStart_r; i < input.r && findingCommand; i++){
                    int lineStart = -1;
                    int lineEnd = -1;
                    for(int j = randomStart_c; j < input.c && findingCommand; j++){
                        if(input.isPainted(i,j) && !used[i][j]){
                            if(lineStart == -1){
                                lineStart = lineEnd = j;
                            }
                            else
                                lineEnd = j;

                            used[i][j] = true;
                        }
                        else{
                            if(lineStart != -1){
                                if(lineEnd != lineStart){
                                    CommandPaintLine c = new CommandPaintLine(i, lineStart, i, lineEnd);
                                    tmpSol.addCommand(c);
                                    //System.out.println("Added HORIZONTAL "+c.toString());
                                    findingCommand = false;
                                }
                                else
                                    used[i][lineEnd] = false;
                                lineStart = -1;
                                lineEnd = -1;
                            }
                        }
                    }

                    if(lineStart != -1){
                        if(lineEnd != lineStart){
                            CommandPaintLine c = new CommandPaintLine(i, lineStart, i, lineEnd);
                            tmpSol.addCommand(c);
                            //System.out.println("Added HORIZONTAL "+c.toString());
                            findingCommand = false;
                        }
                        else
                            used[i][lineEnd] = false;
                    }
                }

                if(findingCommand)
                    useHorizontal = false;

            }
            else if(commandType == 2 && useSquare){//square
                boolean findingCommand = true;
                for(int i = randomStart_r; i < input.r &&findingCommand; i++) {
                    for (int j = randomStart_c; j < input.c && findingCommand; j++) {
                        int maxSquareSize = Math.min(input.c - j, input.r - i);
                        int s = 0;
                        CommandPaintSquare validCommand = null;
                        ArrayList<CommandEraseCell> clearCommands = new ArrayList<>();
                        boolean possibleSolution = true;
                        while(possibleSolution){
                            int l = 2*s +1;
                            int maxClear = useErase ? (int) (s/((Math.min(input.r, input.c)/1.5f))) : 0; // s/3;//s/2/*1? 2? 3?*/; //FIXME problemi con random gen e dataset grande
                            int clearCount = 0;
                            if(l > maxSquareSize)
                                break;
                            boolean validSquare = true;
                            for(int square_r = i; square_r < i+l && validSquare; square_r++){
                                for(int square_c = j; square_c < j+l && validSquare; square_c++){
                                    validSquare = input.isPainted(square_r, square_c) && !used[square_r][square_c];
                                    if(!validSquare && clearCount<maxClear){
                                        clearCount++;
                                        clearCommands.add(new CommandEraseCell(square_r, square_c));
                                        validSquare = true;
                                    }
                                }
                            }

                            if(validSquare){
                                validCommand = new CommandPaintSquare(i+l/2, j+l/2, s);
                                s++;
                            }
                            else
                                possibleSolution = false;

                        }

                        if(validCommand != null){
                            int from_r = validCommand.r - validCommand.s;
                            int to_r = validCommand.r + validCommand.s;
                            int from_c = validCommand.c -validCommand.s;
                            int to_c = validCommand.c + validCommand.s;
                            for(int k = from_r; k <= to_r; k++)
                                for(int h = from_c ; h <= to_c; h++)
                                    used[k][h] = true;

                            tmpSol.addCommand(validCommand);
                            for(Command c:clearCommands)
                                tmpSol.addCommand(c);
                            clearCommands.clear();

                            findingCommand = false;
                        }
                    }
                }
            }
        }
        long tac = System.currentTimeMillis();
        //System.out.println("Created random valid solution in: " + (tac-tic) +" ms. (score " + getScore(tmpSol)+")");

        return tmpSol;
    }

    public boolean[][] getUsedMap(Solution s){
        boolean solMap[][] = new boolean[input.r][input.c];
        for(Command comm : s.getCommands()){
            if(comm instanceof CommandEraseCell){
                solMap[((CommandEraseCell)comm).r][((CommandEraseCell)comm).c] = false;
            }
            else if (comm instanceof CommandPaintLine){
                CommandPaintLine line = (CommandPaintLine)comm;
                for(int i=line.r1; i<=line.r2; i++){
                    for(int j=line.c1; j<=line.c2; j++){
                        solMap[i][j] = true;
                    }
                }
            }
            else if(comm instanceof  CommandPaintSquare){
                CommandPaintSquare square = (CommandPaintSquare)comm;
                for(int i = square.r - square.s ; i <= (square.r + square.s); i++ ){
                    for(int j = square.c - square.s ; j <= (square.c + square.s); j++ ){
                        solMap[i][j] = true;
                    }
                }
            }
        }

        return solMap;
    }

    public boolean checkValidity(Solution s){
        boolean solMap[][] = getUsedMap(s);

        boolean equal = true;
        for(int r=0; r<input.r && equal; r++){
            for(int c=0;c<input.c && equal;c++){
                equal = solMap[r][c] == input.isPainted(r,c);
            }
        }
        return equal;
    }

    public void elaborateSolution(){
        boolean useSquare = true;
        int squareUpperLimit = Math.min(input.r, input.c)/2;

        Solution bestSolution = null;
        int bestScore = 0;

        for(int iter=0;iter<= squareUpperLimit; iter++){
            System.out.println("Iteration: "+iter);
            sol = new Solution();
            boolean used[][] = new boolean[input.r][input.c];
            //Square
            if(useSquare){
                for(int i = 0; i < input.r; i++) {
                    for (int j = 0; j < input.c; j++) {
                        int maxSquareSize = Math.min(input.c - j, input.r - i);
                        int s = iter;
                        CommandPaintSquare validCommand = null;
                        ArrayList<CommandEraseCell> clearCommands = new ArrayList<>();
                        boolean possibleSolution = true;
                        while(possibleSolution){
                            int l = 2*s +1;
                            int maxClear = 0/*1? 2? 3?*/;
                            int clearCount = 0;
                            if(l > maxSquareSize)
                                break;
                            boolean validSquare = true;
                            for(int square_r = i; square_r < i+l && validSquare; square_r++){
                                for(int square_c = j; square_c < j+l && validSquare; square_c++){
                                    validSquare = input.isPainted(square_r, square_c) && !used[square_r][square_c];
                                    if(!validSquare && clearCount<maxClear){
                                        clearCount++;
                                        clearCommands.add(new CommandEraseCell(square_r, square_c));
                                        validSquare = true;
                                    }
                                }
                            }

                            if(validSquare){
                                validCommand = new CommandPaintSquare(i+l/2, j+l/2, s);
                                s++;
                            }
                            else
                                possibleSolution = false;

                        }

                        if(validCommand != null){
                            int from_r = validCommand.r - validCommand.s;
                            int to_r = validCommand.r + validCommand.s;
                            int from_c = validCommand.c -validCommand.s;
                            int to_c = validCommand.c + validCommand.s;
                            for(int k = from_r; k <= to_r; k++)
                                for(int h = from_c ; h <= to_c; h++)
                                    used[k][h] = true;

                            sol.addCommand(validCommand);
                            for(Command c:clearCommands)
                                sol.addCommand(c);
                            clearCommands.clear();
                        }
                    }
                }
            }

            //Horizontal
            for(int i = 0; i < input.r; i++){
                int lineStart = -1;
                int lineEnd = -1;
                for(int j = 0; j < input.c; j++){
                    if(input.isPainted(i,j) && !used[i][j]){
                        if(lineStart == -1){
                            lineStart = lineEnd = j;
                        }
                        else
                            lineEnd = j;

                        used[i][j] = true;
                    }
                    else{
                        if(lineStart != -1){
                            if(lineStart != lineEnd)
                                sol.addCommand(new CommandPaintLine(i, lineStart, i, lineEnd));
                            else
                                used[i][lineStart] = false;

                            lineStart = -1;
                            lineEnd = -1;
                        }
                    }
                }

                if(lineStart != -1){
                    if(lineStart != lineEnd)
                        sol.addCommand(new CommandPaintLine(i, lineStart, i, lineEnd));
                    else
                        used[i][lineStart] = false;
                }
            }

            //Vertical
            for(int i = 0; i < input.c; i++){
                int lineStart = -1;
                int lineEnd = -1;
                for(int j = 0; j < input.r; j++){
                    if(input.isPainted(j, i) && !used[j][i]){
                        if(lineStart == -1){
                            lineStart = lineEnd = j;
                        }
                        else
                            lineEnd = j;

                        used[j][i] = true;
                    }
                    else{
                        if(lineStart != -1){
                            if(lineStart != lineEnd)
                                sol.addCommand(new CommandPaintLine(lineStart, i, lineEnd, i));
                            else
                                used[lineEnd][i] = false;

                            lineStart = -1;
                            lineEnd = -1;
                        }
                    }
                }

                if(lineStart != -1){
                    if(lineStart != lineEnd)
                        sol.addCommand(new CommandPaintLine(lineStart, i, lineEnd, i));
                    else
                        used[lineEnd][i] = false;
                }
            }

            //Missing?
            for(int i = 0; i<input.r; i++){
                for(int j = 0; j<input.c; j++){
                    if(input.isPainted(i, j) && !used[i][j])
                        sol.addCommand(new CommandPaintSquare(i,j,0));
                }
            }


            if(getScore() > bestScore){
                bestSolution = sol;
                bestScore = getScore();
            }
        }

        sol = bestSolution;


    }



    public int getScore(){
        return (input.r * input.c) - sol.getCommandsNumber();
    }

    public int getScore(Solution s){
        return (input.r * input.c) - s.getCommandsNumber();
    }


}
