package org.megadevs.hashcode.painting;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ziby on 06/02/16.
 */
public class Solution implements Comparable<Solution> {

    private ArrayList<Command> solution;

    public Solution(){
        solution = new ArrayList<>();
    }

    public void addCommand(Command c){
        solution.add(c);
    }

    public int getCommandsNumber(){
        return solution.size();
    }

    public ArrayList<Command> getCommands(){
        return solution;
    }

    public String printSolution(){
        StringBuilder sb = new StringBuilder();
        sb.append(solution.size());
        sb.append("\n");
        for(Command c : solution)
            sb.append(c.toString() +"\n");

        return sb.toString();
    }

    public void saveToFile(String fileName){
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter( new FileWriter(fileName));
            writer.write(printSolution());
        }
        catch ( IOException e){
            e.printStackTrace();
        }
        finally    {
            try     {
                if ( writer != null)
                    writer.close( );
            }
            catch ( IOException e)            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int compareTo(Solution o) {
        if(getCommands().size() > o.getCommands().size())
            return 1;
        else if (getCommands().size() == o.getCommands().size())
            return 0;
        return -1;
    }
}
