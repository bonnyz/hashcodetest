package org.megadevs.hashcode.painting;

/**
 * Created by ziby on 06/02/16.
 */
public class CommandPaintSquare extends Command {
    int r;
    int c;
    int s;
    public CommandPaintSquare(int r, int c, int s){
        this.r = r;
        this.c = c;
        this.s = s;
    }

    @Override
    public String toString() {
        return "PAINT_SQUARE " + r + " " + c + " " + s;
    }
}
