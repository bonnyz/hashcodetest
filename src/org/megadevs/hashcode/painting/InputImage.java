package org.megadevs.hashcode.painting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ziby on 06/02/16.
 */
public class InputImage {

    int r;
    int c;
    boolean[][] data;

    public InputImage(int r, int c){
        this.r = r;
        this.c = c;
        data = new boolean[r][c];
        System.out.println("Created input image " + r + " x "+c);
    }

    public boolean isPainted(int rIndex, int cIndex){
        return data[rIndex][cIndex] ;
    }

    public void fillRow(int rIndex, String newRow){
        System.out.println("ROW " +rIndex + "\t"+newRow + " " + newRow.length());
        for(short i = 0; i<c; i++){
            data[rIndex][i] = newRow.charAt(i) == '#';
        }
    }


    public static InputImage loadFromFile(String file){
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = br.readLine();
            String[] split = line.split(" ");
            InputImage ris = new InputImage(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
            int index = 0;
            while ((line = br.readLine()) != null) {
                ris.fillRow(index++, line);
            }
            return ris;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
