package org.megadevs.hashcode.painting;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ziby on 10/02/16.
 */
public class GeneticSolver {

    Solver baseSolver;

    public Solution getBestSolution() {
        return bestSolution;
    }

    Solution bestSolution;
    ArrayList<Solution> population;

    public GeneticSolver(Solver baseSolver){
        this.baseSolver = baseSolver;
        population = new ArrayList<>();
    }

    public void initPopulation(int initialSize, float keepFromSolutionPercentage){
        //baseSolver.elaborateBaseSolution();
        //bestSolution = baseSolver.getSolution();
        //population.add(bestSolution);
        Solution lastSol = bestSolution;
        System.out.println("Creating initial population of "+initialSize + " elements");
        for(int i = population.size(); i < initialSize; i++){
            //System.out.println("creating element " + i);
            lastSol = baseSolver.generateRandomSolution(bestSolution, keepFromSolutionPercentage, true, true, true, false);
            population.add(lastSol);
            if(bestSolution == null || baseSolver.getScore(lastSol) > baseSolver.getScore(bestSolution)){
                bestSolution = lastSol;
                System.out.println("NEW best solution " + baseSolver.getScore(bestSolution));
            }
        }
        System.out.println("Population is ready");
    }


    public void evolve(int numberOfIterations, float mutationFactor){
        for(int i=0; i< numberOfIterations; i++){
            //Collections.shuffle(population);
            Collections.sort(population ,Collections.reverseOrder());
            System.out.println("Evolve iter " +i);

            ArrayList<Solution> newPop = new ArrayList<>();
            Solution localBest = null;
            for(int p = 0; p < population.size()-1; p+=2){

                for(int f =0; f<1; f++){
                    Solution newSol = f == 0 ? crossFitMerge(population.get(p), population.get(p + 1)) : crossFit(population.get(p), population.get(p + 1));

                    if(baseSolver.getScore(newSol)>baseSolver.getScore(bestSolution)){
                        bestSolution = newSol;
                        System.out.println("NEW best solution " + baseSolver.getScore(bestSolution));
                    }

                    if(localBest == null || baseSolver.getScore(newSol)>baseSolver.getScore(localBest)){
                        localBest = newSol;
                    }

                    //keep one parent //FIXME boooh
                    if(baseSolver.randomGenerator.nextDouble() > mutationFactor){
                        newPop.add(baseSolver.randomGenerator.nextBoolean() ? population.get(p) : population.get(p+1));
                    }
                    else
                        newPop.add(baseSolver.generateRandomSolution(newSol, 0.9f, true, true, true, false));

                    //keep the new cross sol
                    newPop.add(newSol);
                }
            }
            System.out.println("iter best solution " + baseSolver.getScore(localBest));

            population = newPop;

        }
    }


    public Solution crossFitOnly(Solution a, Solution b){
        Solution s = new Solution();
        Collections.shuffle(a.getCommands());
        Collections.shuffle(b.getCommands());
        Solution bestParent, otherParent;
        if(a.getCommands().size() < b.getCommands().size()){
            bestParent = a;
            otherParent = b;
        }
        else{
            bestParent = b;
            otherParent = a;
        }

        int bestIndex = 0;
        int otherIndex = 0;
        while(!baseSolver.checkValidity(s)){
            if(baseSolver.randomGenerator.nextDouble() < 0.95 && bestIndex < bestSolution.getCommands().size()){ //pick from best
                s.addCommand(bestParent.getCommands().get(bestIndex++));
            }
            else if(otherIndex < otherParent.getCommands().size()){ //pick from other
                s.addCommand(otherParent.getCommands().get(otherIndex++));
            }
        }
        return s;
    }

    public Solution crossFitB(Solution a, Solution b){
        Solution s = new Solution();
        Collections.shuffle(a.getCommands());
        Collections.shuffle(b.getCommands());
        Solution bestParent;
        if(a.getCommands().size() < b.getCommands().size()){
            bestParent = a;
        }
        else{
            bestParent = b;
        }
        int commandsToSave = (int) (bestParent.getCommands().size()*0.9f);

        for(int i=0; i<commandsToSave; i++){
            s.addCommand(bestParent.getCommands().get(i));
        }

        return baseSolver.generateRandomSolution(s, 1.0f, true, true, true, false);
    }


    public Solution crossFitMerge(Solution a, Solution b){
        Solution s = new Solution();
        for(Command a_command:a.getCommands()){
            for(Command b_command : b.getCommands() ){
                if(Command.areEquivalent(a_command, b_command)){
                    s.addCommand(a_command);
                    break;
                }
            }
        }
        return baseSolver.generateRandomSolution(s, 1.0f, true, true, true, false);
    }


    public Solution crossFit(Solution a, Solution b){
        Solution s = new Solution();
        int bestSol = Math.min(a.getCommands().size(), b.getCommands().size());

        Collections.shuffle(a.getCommands());
        Collections.shuffle(b.getCommands());
        for(int i=0; i<bestSol/3; i++){
            s.addCommand(a.getCommands().get(i));
            s.addCommand(b.getCommands().get(i));
        }

        return baseSolver.generateRandomSolution(s, 1.0f, true, true, true, false);
    }



}
