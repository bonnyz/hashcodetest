package org.megadevs.hashcode.painting;

/**
 * Created by ziby on 06/02/16.
 */
public class Command {



    public static boolean areEquivalent(Command a, Command b){
        if(a instanceof CommandPaintSquare && b instanceof CommandPaintSquare){
            return ((CommandPaintSquare)a).r == ((CommandPaintSquare)b).r && ((CommandPaintSquare)a).c == ((CommandPaintSquare)b).c && ((CommandPaintSquare)a).s == ((CommandPaintSquare)b).s;
        }
        else if(a instanceof CommandPaintLine && b instanceof CommandPaintLine){
            return ((CommandPaintLine)a).r1 == ((CommandPaintLine)b).r1 &&
                    ((CommandPaintLine)a).r2 == ((CommandPaintLine)b).r2 &&
                    ((CommandPaintLine)a).c1 == ((CommandPaintLine)b).c1 &&
                    ((CommandPaintLine)a).c2 == ((CommandPaintLine)b).c2 ;
        }
        else if(a instanceof CommandEraseCell && b instanceof CommandEraseCell){
            return ((CommandEraseCell)a).r == ((CommandEraseCell)b).r && ((CommandEraseCell)a).c == ((CommandEraseCell)b).c;
        }
        return false;
    }

}
