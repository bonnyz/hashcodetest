package org.megadevs.hashcode.painting;

/**
 * Created by ziby on 06/02/16.
 */
public class CommandEraseCell extends Command {
    int r;
    int c;

    public CommandEraseCell(int r, int c){
        this.r = r;
        this.c = c;
    }

    @Override
    public String toString() {
        return "ERASE_CELL " + r + " " + c;
    }
}
