package org.megadevs.hashcode;

import org.megadevs.hashcode.painting.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String input1 = "/home/ziby/Downloads/logo.in";
        String input2 = "/home/ziby/Downloads/learn_and_teach.in";
        String input3 = "/home/ziby/Downloads/right_angle.in";

        String data = input1;

        InputImage inputImage = InputImage.loadFromFile(data);

        Solver solver = new Solver(inputImage);

        /*
        System.out.println("Base solution");
        solver.elaborateBaseSolution();
        System.out.println(solver.getSolution().printSolution());
        System.out.println("Score: " + solver.getScore());
        solver.getSolution().saveToFile(data+".sol");
        System.out.println("-----------------------------------------------------------------------");
        */

        System.out.println("Better solution");
        solver.elaborateSolution();
        System.out.println(solver.getSolution().printSolution());
        System.out.println("Score: " + solver.getScore());
        solver.getSolution().saveToFile(data + ".sol");
        System.out.println("Solution is valid? " + solver.checkValidity(solver.getSolution()));


        //Genetic alg
        GeneticSolver genetic = new GeneticSolver(solver);
        genetic.initPopulation(500, 0);
        genetic.evolve(100000, 0.001f);
        System.out.println("Final genetic best solution: "+solver.getScore(genetic.getBestSolution()));


        /*
        int maxIter = 100;
        int iter=0;
        Solution bestSol = solver.getSolution();
        Solution lastSol = bestSol;
        System.out.println("Looking for random sol...");
        while(iter++ < maxIter){
            lastSol = solver.generateRandomSolution(bestSol, 0.95f ,true, true, true, true);
            if(bestSol == null || solver.getScore(lastSol) > solver.getScore(bestSol)){
                bestSol = lastSol;
                System.out.println("New BEST solution: "+solver.getScore(bestSol));
            }
        }
        //System.out.println(bestSol.printSolution());
        System.out.println("FINISH");*/

        /* test for checkValidity
        System.out.println("Sol is valid? " + solver.checkValidity(solver.getSolution()));
        solver.getSolution().addCommand(new CommandPaintLine(0, 0, 0, 0));
        System.out.println("Sol is valid? " + solver.checkValidity(solver.getSolution()));
        */
        System.out.println("-----------------------------------------------------------------------");
    }
}
